﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;
using System.IO;
using System.Linq;

public class EditorSmartScreenshotTakerWindow : EditorWindow
{
    private const string BASE_FOLDER_NAME = "Screenshots/";

    private static readonly AspectRatio[] aspectRatios = new AspectRatio[]
    {
        new AspectRatio("AppStore Land 6.5 inch (iPhone XS Max, iPhone XR)", 2688, 1242, GameViewSizeType.FixedResolution),
        new AspectRatio("AppStore Land 5.5 inch (iPhone 6s Plus, iPhone 7 Plus, iPhone 8 Plus)", 2208, 1242, GameViewSizeType.FixedResolution),
        new AspectRatio("AppStore Land 12.9 inch (iPad Pro (3rd gen))", 2732, 2048, GameViewSizeType.FixedResolution), //there should be adifference - maybe in dpi 
        //new AspectRatio("AppStore Land 12.9 inch (iPad Pro (2nd gen))", 2732, 2048, GameViewSizeType.FixedResolution), //there should be adifference - maybe in dpi

        new AspectRatio("GooglePlay Land FullHD", 1920, 1080, GameViewSizeType.FixedResolution),
        new AspectRatio("GooglePlay Land 7-inch tablet", 1920, 1200, GameViewSizeType.FixedResolution),  //there should be adifference - maybe in dpi
        //new AspectRatio("GooglePlay Land 10-inch tablet", 1920, 1200, GameViewSizeType.FixedResolution), //there should be adifference - maybe in dpi
    };
    
    private static bool[] activeRatios;

    private static bool isInitialized;

    private IEnumerator screenShotCoroutine;

    private static object gameViewSizesInstance;
    private static MethodInfo getGroup;

    private int[] sizes = new int[] { 1, 2, 4, 8 };
    private string[] sizeNames;
    private int selectedSize = 0;

    private static GameViewSizeGroupType gameViewSizeGroupType;

#if MODULE_MULTILANGUAGE
    private bool useMultilaguage = false;
#endif

    [MenuItem("Window/Smart Screenshot Taker/Screnshot Taker Window")]
    [MenuItem("Tools/Editor/Screnshot Taker")]
    public static EditorSmartScreenshotTakerWindow ShowWindow()
    {
        EditorSmartScreenshotTakerWindow window = GetWindow<EditorSmartScreenshotTakerWindow>(false);
        window.titleContent = new GUIContent("Smart Screenshot Taker");
        
        return window;
    }

    [MenuItem("Window/Smart Screenshot Taker/Take Screenshot _F12")]
    public static void TakeScreenshotShortcut()
    {
        EditorSmartScreenshotTakerWindow window = ShowWindow();

        if (Application.isPlaying)
        {
            if (window != null)
            {
                window.TakeScreenshot();
            }
        }
    }

    private void OnEnable()
    {
        Initialize();

        sizeNames = sizes.Select(x => x.ToString()).ToArray();
    }

    public void TakeScreenshot()
    {
        if (screenShotCoroutine == null)
        {
            if (isInitialized && activeRatios.Where(x => x).Count() > 0)
            {
                EditorApplication.update += EditorUpdate;

                screenShotCoroutine = TakeScreenshotEnumerator();
            }
        }
        else
        {
            Debug.LogWarning("Screenshot Taker is already in work. Please wait.");
        }
    }

    private void EditorUpdate()
    {
        if(screenShotCoroutine != null)
        {
            if(!screenShotCoroutine.MoveNext())
            {
                EditorApplication.update -= EditorUpdate;

                screenShotCoroutine = null;
            }
        }
    }
    public static GameViewSizeGroupType GetCurrentGroupType()
    {
        var getCurrentGroupTypeProp = gameViewSizesInstance.GetType().GetProperty("currentGroupType");
        return (GameViewSizeGroupType)(int)getCurrentGroupTypeProp.GetValue(gameViewSizesInstance, null);
    }

    private IEnumerator TakeScreenshotEnumerator()
    {
        List<Camera> cameras = new List<Camera>(Camera.allCameras);
        cameras.OrderBy(x => x.depth);

        float timeScale = Time.timeScale;
        Time.timeScale = 0;
        
        Assembly assembly = Assembly.GetAssembly(typeof(ScreenshotResizeAttribute));
        IEnumerable<MethodInfo> methods = assembly.GetTypes().SelectMany(m => m.GetMethods().Where(i => i.GetCustomAttributes(typeof(ScreenshotResizeAttribute), false).Count() > 0));
        Dictionary<MethodInfo, UnityEngine.Object[]> cachableObjects = new Dictionary<MethodInfo, UnityEngine.Object[]>();
        
        EditorWindow window = FullscreenManger.OpenFullscreen(EditorWindow.GetWindow(typeof(Editor).Assembly.GetType("UnityEditor.GameView")));

        yield return null;

        foreach (var method in methods)
        {
            UnityEngine.Object[] objects = FindObjectsOfType(method.ReflectedType);
            if (objects != null)
            {
                cachableObjects.Add(method, objects);
            }
        }
        
        for (int i = 0; i < aspectRatios.Length; i++)
        {
            if (activeRatios[i])
            {
#if MODULE_MULTILANGUAGE
                if (useMultilaguage)
                {
                    Multilanguage.ProjectLanguages[] projectLanguages = Multilanguage.ActiveLanguages();
                    for(int j = 0; j < projectLanguages.Length; j++)
                    {
                        Multilanguage.ChangeLanguage(projectLanguages[j]);

                        yield return null;
                                                
                        int sizeIndex = FindSize(gameViewSizeGroupType, aspectRatios[i].name);
                        if (sizeIndex != -1)
                            SetSize(sizeIndex);

                        yield return null;

                        foreach (var cachableObject in cachableObjects)
                        {
                            for (int c = 0; c < cachableObject.Value.Length; c++)
                            {
                                cachableObject.Key.Invoke(cachableObject.Value[c], null);
                            }
                        }

                        window.Repaint();

                        yield return null;

                        int width = aspectRatios[i].xAspect * sizes[selectedSize];
                        int height = aspectRatios[i].yAspect * sizes[selectedSize];

                        RenderTexture renderTexture = RenderTexture.GetTemporary(width, height, 24);
                        RenderTexture.active = renderTexture;

                        foreach (Camera camera in cameras)
                        {
                            if (camera.enabled)
                            {
                                float fov = camera.fieldOfView;
                                camera.targetTexture = renderTexture;
                                camera.Render();
                                camera.targetTexture = null;
                                camera.fieldOfView = fov;
                            }
                        }

                        Texture2D result = new Texture2D(width, height, TextureFormat.RGB24, true);
                        result.ReadPixels(new Rect(0.0f, 0.0f, width, height), 0, 0, true);
                        result.Apply();

                        RenderTexture.active = null;
                        RenderTexture.ReleaseTemporary(renderTexture);

                        byte[] bytes = result.EncodeToPNG();

                        File.WriteAllBytes(BASE_FOLDER_NAME + aspectRatios[i].name + "/Sccreenshot_" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + "_" + projectLanguages[j].ToString() + ".png", bytes);

                        yield return null;
                    }
                }
                else
                {
                    int sizeIndex = FindSize(gameViewSizeGroupType, aspectRatios[i].name);
                    if (sizeIndex != -1)
                        SetSize(sizeIndex);

                    yield return null;

                    foreach (var cachableObject in cachableObjects)
                    {
                        for (int c = 0; c < cachableObject.Value.Length; c++)
                        {
                            cachableObject.Key.Invoke(cachableObject.Value[c], null);
                        }
                    }

                    window.Repaint();

                    yield return null;

                    int width = aspectRatios[i].xAspect * sizes[selectedSize];
                    int height = aspectRatios[i].yAspect * sizes[selectedSize];

                    RenderTexture renderTexture = RenderTexture.GetTemporary(width, height, 24);
                    RenderTexture.active = renderTexture;

                    foreach (Camera camera in cameras)
                    {
                        if (camera.enabled)
                        {
                            float fov = camera.fieldOfView;
                            camera.targetTexture = renderTexture;
                            camera.Render();
                            camera.targetTexture = null;
                            camera.fieldOfView = fov;
                        }
                    }

                    Texture2D result = new Texture2D(width, height, TextureFormat.RGB24, true);
                    result.ReadPixels(new Rect(0.0f, 0.0f, width, height), 0, 0, true);
                    result.Apply();

                    RenderTexture.active = null;
                    RenderTexture.ReleaseTemporary(renderTexture);

                    byte[] bytes = result.EncodeToPNG();

                    File.WriteAllBytes(BASE_FOLDER_NAME + aspectRatios[i].name + "/Sccreenshot_" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".png", bytes);
                }
#else
                int sizeIndex = FindSize(gameViewSizeGroupType, aspectRatios[i].name);
                if (sizeIndex != -1)
                    SetSize(sizeIndex);

                yield return null;

                foreach (var cachableObject in cachableObjects)
                {
                    for (int c = 0; c < cachableObject.Value.Length; c++)
                    {
                        cachableObject.Key.Invoke(cachableObject.Value[c], null);
                    }
                }

                window.Repaint();

                yield return null;

                int width = aspectRatios[i].xAspect * sizes[selectedSize];
                int height = aspectRatios[i].yAspect * sizes[selectedSize];

                RenderTexture renderTexture = RenderTexture.GetTemporary(width, height, 24);
                RenderTexture.active = renderTexture;

                foreach (Camera camera in cameras)
                {
                    if (camera.enabled)
                    {
                        float fov = camera.fieldOfView;
                        camera.targetTexture = renderTexture;
                        camera.Render();
                        camera.targetTexture = null;
                        camera.fieldOfView = fov;
                    }
                }

                Texture2D result = new Texture2D(width, height, TextureFormat.RGB24, true);
                result.ReadPixels(new Rect(0.0f, 0.0f, width, height), 0, 0, true);
                result.Apply();

                RenderTexture.active = null;
                RenderTexture.ReleaseTemporary(renderTexture);

                byte[] bytes = result.EncodeToPNG();

                File.WriteAllBytes(BASE_FOLDER_NAME + aspectRatios[i].name + "/Sccreenshot_" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".png", bytes);
#endif
            }
        }

        FullscreenManger.CloseWindow();

        Time.timeScale = timeScale;
    }
    
    private static void Initialize()
    {
        Type sizesType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSizes");
        Type singleType = typeof(ScriptableSingleton<>).MakeGenericType(sizesType);
        PropertyInfo instanceProp = singleType.GetProperty("instance");
        getGroup = sizesType.GetMethod("GetGroup");
        gameViewSizesInstance = instanceProp.GetValue(null, null);

        gameViewSizeGroupType = GetCurrentGroupType();

        activeRatios = new bool[aspectRatios.Length];
        for (int i = 0; i < aspectRatios.Length; i++)
        {
            if (!SizeExists(gameViewSizeGroupType, aspectRatios[i].name))
            {
                AddCustomSize(aspectRatios[i].sizeType, gameViewSizeGroupType, aspectRatios[i].xAspect, aspectRatios[i].yAspect, aspectRatios[i].name);
            }
        }

        isInitialized = true;
    }

    private void OnGUI()
    {
        if (Application.isPlaying)
        {
            if (!isInitialized)
                Initialize();
            
            EditorGUILayout.BeginVertical(GUI.skin.box);
            for (int i = 0; i < activeRatios.Length; i++)
            {
                EditorGUI.BeginChangeCheck();
                activeRatios[i] = EditorGUILayout.Toggle(new GUIContent(aspectRatios[i].name), activeRatios[i]);
                if (EditorGUI.EndChangeCheck())
                {
                    string folderPath = Application.dataPath.Replace("Assets", "") + BASE_FOLDER_NAME + aspectRatios[i].name + "/";
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                }
            }
            EditorGUILayout.EndVertical();

            selectedSize = EditorGUILayout.Popup("Extra size: ", selectedSize, sizeNames);

#if MODULE_MULTILANGUAGE
            useMultilaguage = EditorGUILayout.Toggle("Use Multilanguage: ", useMultilaguage);
#endif

            if (GUILayout.Button("Take Screenshot"))
            {
                TakeScreenshot();
            }

            Event e = Event.current;
            switch (e.type)
            {
                case EventType.KeyDown:
                    {
                        if (Event.current.keyCode == (KeyCode.F12))
                        {
                            TakeScreenshot();
                        }
                        break;
                    }
            }
        }
        else
        {
            EditorGUILayout.HelpBox("Screenshot Taker works only in play mode.", MessageType.Info);
        }
    }

    public static void SetSize(int index)
    {
        Type gvWndType = typeof(Editor).Assembly.GetType("UnityEditor.GameView");
        PropertyInfo selectedSizeIndexProp = gvWndType.GetProperty("selectedSizeIndex",
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        var gvWnd = EditorWindow.GetWindow(gvWndType);
        selectedSizeIndexProp.SetValue(gvWnd, index, null);
    }
    
    public static void AddCustomSize(GameViewSizeType viewSizeType, GameViewSizeGroupType sizeGroupType, int width, int height, string text)
    {
        System.Object group = GetGroup(sizeGroupType);
        MethodInfo addCustomSize = getGroup.ReturnType.GetMethod("AddCustomSize"); // or group.GetType().
        Type gvsType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSize");
        //ConstructorInfo ctor = gvsType.GetConstructor(new Type[] { typeof(int), typeof(int), typeof(int), typeof(string) });
        ConstructorInfo ctor = gvsType.GetConstructors()[0];

        System.Object newSize = ctor.Invoke(new object[] { (int)viewSizeType, width, height, text });
        addCustomSize.Invoke(group, new object[] { newSize });
    }

    public static bool SizeExists(GameViewSizeGroupType sizeGroupType, string text)
    {
        return FindSize(sizeGroupType, text) != -1;
    }

    public static int FindSize(GameViewSizeGroupType sizeGroupType, string text)
    {
        var group = GetGroup(sizeGroupType);
        var getDisplayTexts = group.GetType().GetMethod("GetDisplayTexts");
        var displayTexts = getDisplayTexts.Invoke(group, null) as string[];
        for (int i = 0; i < displayTexts.Length; i++)
        {
            string display = displayTexts[i];
            int pren = display.IndexOf('(');
            if (pren != -1)
                display = display.Substring(0, pren - 1); // -1 to remove the space that's before the prens. This is very implementation-depdenent
            if (display == text)
                return i;
        }
        return -1;
    }

    public static bool SizeExists(GameViewSizeGroupType sizeGroupType, int width, int height)
    {
        return FindSize(sizeGroupType, width, height) != -1;
    }

    public static int FindSize(GameViewSizeGroupType sizeGroupType, int width, int height)
    {
        // goal:
        // GameViewSizes group = gameViewSizesInstance.GetGroup(sizeGroupType);
        // int sizesCount = group.GetBuiltinCount() + group.GetCustomCount();
        // iterate through the sizes via group.GetGameViewSize(int index)

        var group = GetGroup(sizeGroupType);
        var groupType = group.GetType();
        var getBuiltinCount = groupType.GetMethod("GetBuiltinCount");
        var getCustomCount = groupType.GetMethod("GetCustomCount");
        int sizesCount = (int)getBuiltinCount.Invoke(group, null) + (int)getCustomCount.Invoke(group, null);
        var getGameViewSize = groupType.GetMethod("GetGameViewSize");
        var gvsType = getGameViewSize.ReturnType;
        var widthProp = gvsType.GetProperty("width");
        var heightProp = gvsType.GetProperty("height");
        var indexValue = new object[1];
        for (int i = 0; i < sizesCount; i++)
        {
            indexValue[0] = i;
            var size = getGameViewSize.Invoke(group, indexValue);
            int sizeWidth = (int)widthProp.GetValue(size, null);
            int sizeHeight = (int)heightProp.GetValue(size, null);
            if (sizeWidth == width && sizeHeight == height)
                return i;
        }
        return -1;
    }

    static object GetGroup(GameViewSizeGroupType type)
    {
        return getGroup.Invoke(gameViewSizesInstance, new object[] { (int)type });
    }

    [InitializeOnLoadMethod]
    public static void InitializeScreenshotTaker()
    {
        Initialize();

        string fullPath = Application.dataPath.Replace("Assets", "") + BASE_FOLDER_NAME;
        if (!Directory.Exists(fullPath))
            Directory.CreateDirectory(fullPath);
    }

    public enum GameViewSizeType
    {
        AspectRatio, FixedResolution
    }

    private class AspectRatio
    {
        public string name;
        public int xAspect;
        public int yAspect;
        public GameViewSizeType sizeType;

        public AspectRatio(string name, int xAspect, int yAspect, GameViewSizeType sizeType)
        {
            this.name = name;
            this.xAspect = xAspect;
            this.yAspect = yAspect;
            this.sizeType = sizeType;
        }
    }
}
