﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Vuforia;
using Watermelon;

public class AppController : MonoBehaviour, ITrackableEventHandler
{
    public static AppController instance;

    [Header("References")]
    public UIController uiController;
    public TrackableBehaviour imageTarget;

    public CharacterBehaviour maleCharacter;
    public CharacterBehaviour femaleCharacter;

    [Space(5)]
    public AudioSource phraseAudioSource;
    public AudioSource fxAudioSource;
    public AudioClip shotSound;

    private CharacterBehaviour currentCharacter;
    private CharacterBehaviour.Gender currentCharacterGender;

    private Vector2 firstPressPos;
    private Vector2 secondPressPos;
    private Vector2 currentSwipe;

    private bool swipeDetectionActive;

    private void Awake()
    {
        instance = this;

        //VuforiaRuntime.Instance.InitVuforia();

        swipeDetectionActive = false;
        currentCharacterGender = CharacterBehaviour.Gender.Female;

        femaleCharacter.gameObject.SetActive(false);
        maleCharacter.gameObject.SetActive(false);

        currentCharacter = femaleCharacter;

        //imageTarget.RegisterTrackableEventHandler(this);

    }

    private void OnDestroy()
    {
        imageTarget.UnregisterTrackableEventHandler(this);
    }

    private void Start()
    {
        uiController.ShowBasicTip();
        CheckForPhotoModeCompability();

        NewsController.instance.LoadNews();
    }

    private void Update()
    {
        if (swipeDetectionActive)
        {
            SwipeDetection();
        }
    }

    private void CheckForPhotoModeCompability()
    {
        bool photoModeSupported = true;

        photoModeSupported = (Directory.Exists("/sdcard/DCIM/camera/") || Directory.Exists("/sdcard/media/images/") || Directory.Exists("/sdcard/"));
        Debug.Log("[App Controller] Taking photo is " + (photoModeSupported ? "" : "not ") + "supported.");

        if (!photoModeSupported)
        {
            uiController.DisablePhotoModeUI();
        }
    }

    public void OnTargetFound()
    {
        uiController.ShowARInterface();

        currentCharacter.DisableCharacter();
        currentCharacter = currentCharacterGender == CharacterBehaviour.Gender.Male ? maleCharacter : femaleCharacter;
        currentCharacter.EnableCharacter(true);

        swipeDetectionActive = true;
    }

    public void OnTargetLost()
    {
        uiController.ShowBasicTip();
        currentCharacter.DisableCharacter();

        swipeDetectionActive = false;
    }

    [ContextMenu("Change character")]
    public void ChangeCharacter()
    {
        bool isCharacterTalking = currentCharacter.isTalking;
        currentCharacter.DisableCharacter();

        currentCharacterGender = currentCharacterGender == CharacterBehaviour.Gender.Male ? CharacterBehaviour.Gender.Female : CharacterBehaviour.Gender.Male;
        currentCharacter = currentCharacterGender == CharacterBehaviour.Gender.Male ? maleCharacter : femaleCharacter;

        currentCharacter.EnableCharacter(isCharacterTalking);
    }

    public void PlayPhrase(AudioClip sound)
    {
        phraseAudioSource.Stop();
        phraseAudioSource.clip = sound;
        phraseAudioSource.Play();

        uiController.HideInfoButton();
    }

    private void PlayShotSound()
    {
        fxAudioSource.Stop();
        fxAudioSource.clip = shotSound;
        fxAudioSource.Play();
    }

    public void StopPhrase()
    {
        phraseAudioSource.Stop();
        uiController.ShowInfoButton();
    }

    public void SwipeDetection()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }
            if (t.phase == TouchPhase.Ended)
            {
                secondPressPos = new Vector2(t.position.x, t.position.y);
                currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                Vector3 swipeNormalized = currentSwipe;
                swipeNormalized.Normalize();

                //swipe left
                if (currentSwipe.x < -150f && swipeNormalized.y > -0.5f && swipeNormalized.y < 0.5f)
                {
                    ChangeCharacter();
                }
                //swipe right
                if (currentSwipe.x > 150f && swipeNormalized.y > -0.5f && swipeNormalized.y < 0.5f)
                {
                    ChangeCharacter();
                }
            }
        }
    }

    public void OnInfoButonPressed()
    {
        currentCharacter.StartTalking();
    }

    public void StartPhotoMode()
    {
        currentCharacter.StopTalking();
        uiController.ShowPhotoModeUI();
    }

    public void ClosePhotoMode()
    {
        uiController.ShowARInterface();
    }

    public void MakeShot()
    {
        //uiController.DisableCanvas();

        List<Camera> cameras = new List<Camera>(Camera.allCameras);
        cameras.OrderBy(x => x.depth);

        int width = Screen.width;
        int height = Screen.height;

        RenderTexture renderTexture = RenderTexture.GetTemporary(width, height, 24);
        RenderTexture.active = renderTexture;

        foreach (Camera camera in cameras)
        {
            if (camera.enabled)
            {
                float fov = camera.fieldOfView;
                camera.targetTexture = renderTexture;
                camera.Render();
                camera.targetTexture = null;
                camera.fieldOfView = fov;
            }
        }

        Texture2D result = new Texture2D(width, height, TextureFormat.ARGB32, true);
        result.ReadPixels(new Rect(0.0f, 0.0f, width, height), 0, 0, true);
        result.Apply();

        RenderTexture.active = null;
        RenderTexture.ReleaseTemporary(renderTexture);

        byte[] bytes = result.EncodeToPNG();

        if (Directory.Exists("/sdcard/DCIM/camera/"))
        {
            File.WriteAllBytes("/sdcard/DCIM/camera/CHNU FMI " + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".png", bytes);
        }
        else if (Directory.Exists("/sdcard/media/images/"))
        {
            File.WriteAllBytes("/sdcard/media/images/CHNU FMI " + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".png", bytes);

        }
        else if (Directory.Exists("/sdcard/"))
        {
            Directory.CreateDirectory("/sdcard/CHNU FMI/");
            File.WriteAllBytes("/sdcard/CHNU FMI/CHNU FMI " + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".png", bytes);
        }

        PlayShotSound();

        //uiController.EnableCanvas();
    }

    public void ChangeCharacterPose()
    {
        currentCharacter.ChangePose();
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        Debug.Log("[App Controller] Trackable state changed: " + newStatus);

        if (newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTargetFound();
        }
        else if (newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            OnTargetLost();
        }
    }
}
