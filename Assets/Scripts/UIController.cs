﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController instance;

    public AppController appController;

    [Header("Main UI")]
    public GameObject canvas;
    public GameObject basicTipPanel;
    public GameObject arPanel;
    public GameObject photoModePanel;
    public GameObject infoButton;
    public GameObject openPhotoModeButton;

    [Header("News UI")]
    public GameObject newsPanel;
    public GameObject noNewsLableObject;
    public RectTransform newsPreviewsContrainerTransform;
    public GameObject newsPreviewPanelPrefab;

    public void Awake()
    {
        instance = this;
    }

    #region Main UI

    public void EnableCanvas()
    {
        canvas.SetActive(true);
    }

    public void DisableCanvas()
    {
        canvas.SetActive(false);
    }

    public void ShowBasicTip()
    {
        basicTipPanel.SetActive(true);

        arPanel.SetActive(false);
        photoModePanel.SetActive(false);
    }

    public void ShowARInterface()
    {
        arPanel.SetActive(true);

        basicTipPanel.SetActive(false);
        photoModePanel.SetActive(false);
    }

    public void ShowPhotoModeUI()
    {
        photoModePanel.SetActive(true);
        arPanel.SetActive(false);
    }

    public void ButtonPhotoMode()
    {
        appController.StartPhotoMode();
    }

    public void ButtonBackToARPanel()
    {
        appController.ClosePhotoMode();
    }

    public void ButtonMakeShot()
    {
        appController.MakeShot();
    }

    public void ButtonPose()
    {
        appController.ChangeCharacterPose();
    }

    public void ButtonInfo()
    {
        appController.OnInfoButonPressed();
    }

    public void ShowInfoButton()
    {
        infoButton.SetActive(true);
    }

    public void HideInfoButton()
    {
        infoButton.SetActive(false);
    }

    public void DisablePhotoModeUI()
    {
        openPhotoModeButton.SetActive(false);
    }

    #endregion

    #region News UI

    public void SetNoNewsActiveState(bool isActive)
    {
        noNewsLableObject.SetActive(isActive);
    }

    public void ButtonOpenNewsPanel()
    {
        newsPanel.SetActive(true);
    }

    public void ButtonCloseNewsPanel()
    {
        newsPanel.SetActive(false);
    }

    public void InitNewsPreviewsPanelHeight(int newsAmount)
    {
        float newHeight = newsPreviewPanelPrefab.GetComponent<RectTransform>().sizeDelta.y * newsAmount + newsPreviewsContrainerTransform.GetComponent<VerticalLayoutGroup>().spacing * newsAmount;
        newsPreviewsContrainerTransform.sizeDelta = new Vector2(newsPreviewsContrainerTransform.sizeDelta.x, newHeight);
    }

    #endregion
}