﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using Watermelon.Pool;

public class NewsController : MonoBehaviour
{
    public static NewsController instance;

    [Header("Settings")]
    public string spreadSheetId = "1nEunKALFrKXLHdDUywgXAg1Yjc80HDURu9eXc3o1hDE";

    [Header("UI Refs")]
    public NewsPopUp unreadNewsPopUp;

    private static readonly string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
    private static readonly string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";
    private static readonly char[] TRIM_CHARS = { '\"' };

    private List<NewsData> newsList = new List<NewsData>();
    private WatchedNewsSave watchedNewsSave;

    private Pool newsPreviewPool;
    private Pool newsPanelPool;

    private int unreadNewsAmount = 0;

    [Header("Developement")]
    public bool loadFromFileInEditor = false;
    public TextAsset csvExampleAsset;

    public void Awake()
    {
        instance = this;

        newsPreviewPool = PoolManager.GetPoolByName("NewsPreviewPanel");
        newsPanelPool = PoolManager.GetPoolByName("NewsPanel");

        watchedNewsSave = WatchedNewsSave.Load();

        Debug.Log("Found wathched news: " + watchedNewsSave.newsList.Count);
    }

    [ContextMenu("Load News")]
    public void LoadNews()
    {
#if UNITY_EDITOR
        if (loadFromFileInEditor)
        {
            OnDataDownloaded(csvExampleAsset.text);
            return;
        }
#endif

        StartCoroutine(DownloadCSVCoroutine(spreadSheetId, (csv) => OnDataDownloaded(csv)));
    }

    private IEnumerator DownloadCSVCoroutine(string docId, Action<string> callback)
    {
        string url = @"https://docs.google.com/spreadsheets/d/" + docId + @"/export?format=csv";

        Debug.Log("[News Controller] Connecting to URL: " + url);

        WWWForm form = new WWWForm();
        WWW download = new WWW(url, form);

        yield return download;

        if (!string.IsNullOrEmpty(download.error))
        {
            Debug.Log("[News Controller] Error downloading: " + download.error);
            callback(string.Empty);
        }
        else
        {
            callback(download.text);
        }
    }

    private void OnDataDownloaded(string csv)
    {
        List<List<string>> list = ParseCSV(csv);
        newsList = new List<NewsData>();

        for (int i = 0; i < list.Count; i++)
        {
            NewsData news = new NewsData();

            for (int j = 0; j < list[i].Count; j++)
            {
                PopulateNews(news, j, list[i][j]);
            }

            newsList.Add(news);
        }

        Debug.Log("[News Controller] News loaded. Amount: " + newsList.Count);

        UIController.instance.SetNoNewsActiveState(newsList.Count == 0);
        UIController.instance.InitNewsPreviewsPanelHeight(newsList.Count);

        unreadNewsAmount = 0;

        for (int i = 0; i < newsList.Count; i++)
        {
            bool isUnread = true;
            for (int j = 0; j < watchedNewsSave.newsList.Count; j++)
            {
                if(watchedNewsSave.newsList[j].date.Equals(newsList[i].date) && watchedNewsSave.newsList[j].topic.Equals(newsList[i].topic))
                {
                    isUnread = false;
                    break;
                }
            }

            unreadNewsAmount += isUnread ? 1 : 0;

            newsPreviewPool.GetPooledObject().GetComponent<NewsPreviewBehaviour>().Init(newsList[i], isUnread);
        }

        unreadNewsPopUp.DisplayPopUp(unreadNewsAmount);
    }

    private List<List<string>> ParseCSV(string text)
    {
        text = CleanReturnInCsvTexts(text);

        List<List<string>> list = new List<List<string>>();
        string[] lines = Regex.Split(text, LINE_SPLIT_RE);

        if (lines.Length <= 1)
            return list;

        string[] header = Regex.Split(lines[0], SPLIT_RE);
        bool jumpedFirst = false;

        foreach (string line in lines)
        {
            if (!jumpedFirst)
            {
                jumpedFirst = true;
                continue;
            }

            string[] values = Regex.Split(line, SPLIT_RE);
            List<string> entry = new List<string>();

            for (int j = 0; j < header.Length && j < values.Length; j++)
            {
                string value = values[j];
                value = DecodeSpecialCharsFromCSV(value);
                entry.Add(value);
            }

            list.Add(entry);
        }

        return list;
    }

    private string CleanReturnInCsvTexts(string text)
    {
        text = text.Replace("\"\"", "'");

        if (text.IndexOf("\"") > -1)
        {
            string clean = "";
            bool insideQuote = false;

            for (int j = 0; j < text.Length; j++)
            {
                if (!insideQuote && text[j] == '\"')
                {
                    insideQuote = true;
                }
                else if (insideQuote && text[j] == '\"')
                {
                    insideQuote = false;
                }
                else if (insideQuote)
                {
                    if (text[j] == '\n')
                    {
                        clean += "<br>";
                    }
                    else if (text[j] == ',')
                    {
                        clean += "<c>";
                    }
                    else
                    {
                        clean += text[j];
                    }
                }
                else
                {
                    clean += text[j];
                }
            }

            text = clean;
        }

        return text;
    }

    private void PopulateNews(NewsData newsData, int columnIndex, string columnData)
    {
        if (columnIndex < 0 || columnIndex > 2)
        {
            Debug.LogError("Column index is out of range. Value: " + columnIndex);
            return;
        }

        if (columnIndex == 0)
        {
            newsData.date = columnData;
        }

        if (columnIndex == 1)
        {
            newsData.topic = columnData;
        }

        if (columnIndex == 2)
        {
            newsData.body = columnData;

            if (columnData.Contains("https://") || columnData.Contains("http://"))
            {
                Debug.Log("found addres");
                var result = Regex.Matches(columnData, @"(http|ftp|https):\/\/([\w\-_]+(?:(?:\.[\w\-_]+)+))([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");
                if (result.Count > 0)
                {
                    newsData.hyperLink = result[0].Value.ToString();
                }
            }
        }
    }

    private string DecodeSpecialCharsFromCSV(string value)
    {
        value = value.TrimStart(TRIM_CHARS).TrimEnd(TRIM_CHARS).Replace("\\", "").Replace("<br>", "\n").Replace("<c>", ",");
        return value;
    }

    public void OnNewsPreviewClicked(RectTransform previewRect, NewsData newsData, bool isUnread)
    {
        GameObject newsPanel = newsPanelPool.GetPooledObject();
        newsPanel.GetComponent<RectTransform>().position = previewRect.position;
        newsPanel.GetComponent<NewsPanelBehaviour>().Init(newsData, isUnread);

        if(isUnread)
        {
            watchedNewsSave.newsList.Add(newsData);
            watchedNewsSave.Save();

            unreadNewsAmount--;

            unreadNewsPopUp.DisplayPopUp(unreadNewsAmount);
        }
    }
}