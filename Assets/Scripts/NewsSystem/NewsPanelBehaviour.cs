﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Watermelon;
using Watermelon.Core;

public class NewsPanelBehaviour : MonoBehaviour
{
    public RectTransform rectTransformRef;
    public Text dateText;
    public RectTransform dateRect;
    public Text topicText;
    public RectTransform topicRect;
    public Text bodyText;
    public RectTransform bodyScrollRect;

    [Space(5f)]
    public GameObject unreadLableObject;
    public Image unreadLableBackImage;
    public Text unreadLableText;
    public GameObject backButtonObject;
    public Image backButtonImage;
    public Text backButtonText;
    public GameObject hyperLinkButtonObject;
    public Image hyperLinkButtonImage;
    public Text hyperLinkButtonText;

    private NewsData newsData;

    public void Init(NewsData newsData, bool isUnread)
    {
        this.newsData = newsData;

        dateText.text = newsData.date;
        topicText.text = newsData.topic;
        bodyText.text = newsData.body;

        float animationTime = 0.5f;

        Tween.DoFloat(rectTransformRef.sizeDelta.x, 0f, animationTime, (float f) => rectTransformRef.sizeDelta = rectTransformRef.sizeDelta.SetX(f)).SetEasing(Ease.Type.CubicIn);
        Tween.DoFloat(rectTransformRef.sizeDelta.y, 0f, animationTime, (float f) => rectTransformRef.sizeDelta = rectTransformRef.sizeDelta.SetY(f)).SetEasing(Ease.Type.CubicIn);
        Tween.DoFloat(rectTransformRef.anchoredPosition.y, 0f, animationTime, (float f) => rectTransformRef.anchoredPosition = rectTransformRef.anchoredPosition.SetY(f)).SetEasing(Ease.Type.CubicIn);

        unreadLableObject.SetActive(isUnread);

        if (isUnread)
        {
            unreadLableBackImage.color = unreadLableBackImage.color.SetAlpha(1f);
            unreadLableText.color = unreadLableText.color.SetAlpha(1f);

            unreadLableBackImage.DOFade(0f, animationTime);
            unreadLableText.DOFade(0f, animationTime);
        }

        Vector2 prevPosition = dateRect.position;

        dateRect.anchorMin = Vector3.one;
        dateRect.anchorMax = Vector3.one;
        dateRect.pivot = Vector2.one;
        dateText.alignment = TextAnchor.MiddleRight;

        dateRect.position = prevPosition;
        dateRect.anchoredPosition = dateRect.anchoredPosition.AddToX(dateRect.sizeDelta.x * 0.5f);

        Tween.DoFloat(dateRect.anchoredPosition.x, -35f, animationTime, (float f) => dateRect.anchoredPosition = dateRect.anchoredPosition.SetX(f)).SetEasing(Ease.Type.CubicOut);
        Tween.DoFloat(dateRect.anchoredPosition.y, -85f, animationTime, (float f) => dateRect.anchoredPosition = dateRect.anchoredPosition.SetY(f)).SetEasing(Ease.Type.CubicOut);
        Tween.DoFloat(topicRect.anchoredPosition.y, -100f, animationTime, (float f) => topicRect.anchoredPosition = topicRect.anchoredPosition.SetY(f)).SetEasing(Ease.Type.CubicOut);

        if (newsData.hyperLink == string.Empty)
        {
            bodyScrollRect.anchoredPosition = new Vector2(0f, 60f);
            bodyScrollRect.sizeDelta = new Vector2(1010f, 1637f);
        }
        else
        {
            bodyScrollRect.anchoredPosition = new Vector2(0f, 153.5f);
            bodyScrollRect.sizeDelta = new Vector2(1010f, 1450f);
        }

        float textAppearingTime = 0.4f;

        Tween.DelayedCall(animationTime, () =>
         {
             Tween.DoFloat(bodyText.color.a, 1f, textAppearingTime, (float f) => bodyText.color = bodyText.color.SetAlpha(f));

             backButtonImage.DOFade(1f, textAppearingTime);
             backButtonText.DOFade(1f, textAppearingTime);

             hyperLinkButtonObject.SetActive(newsData.hyperLink != string.Empty);

             if (hyperLinkButtonObject.activeSelf)
             {
                 hyperLinkButtonImage.DOFade(1f, textAppearingTime);
                 hyperLinkButtonText.DOFade(1f, textAppearingTime);
             }
         });
    }


    public void BackButton()
    {
        rectTransformRef.sizeDelta = new Vector2(-200f, -1908f);

        dateRect.anchorMax = Vector2.up;
        dateRect.anchorMin = Vector2.up;
        dateRect.pivot = Vector2.up;
        dateText.alignment = TextAnchor.MiddleLeft;

        dateRect.anchoredPosition = new Vector2(35f, -10f);
        topicRect.anchoredPosition = topicRect.anchoredPosition.SetY(-140f);
        bodyText.color = bodyText.color.SetAlpha(0f);

        backButtonImage.color = backButtonImage.color.SetAlpha(0f);
        backButtonText.color = backButtonText.color.SetAlpha(0f);

        hyperLinkButtonImage.color = backButtonImage.color.SetAlpha(0f);
        hyperLinkButtonText.color = backButtonText.color.SetAlpha(0f);


        gameObject.SetActive(false);
    }

    public void HyperLinkButton()
    {
        Application.OpenURL(newsData.hyperLink);
    }
}