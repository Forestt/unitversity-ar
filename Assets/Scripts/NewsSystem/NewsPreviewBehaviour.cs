﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewsPreviewBehaviour : MonoBehaviour
{
    public RectTransform rectTransformRef;
    public Text dateText;
    public Text topicText;

    public GameObject unreadLableObject;

    private NewsData newsData;
    private bool isUnread;

    public void Init(NewsData newsData, bool isUnread)
    {
        Debug.Log("Init: " + newsData.topic + "   " + isUnread);
        dateText.text = newsData.date;
        topicText.text = newsData.topic;

        unreadLableObject.SetActive(isUnread);

        this.newsData = newsData;
        this.isUnread = isUnread;
    }

    public void OnClick()
    {
        NewsController.instance.OnNewsPreviewClicked(rectTransformRef, newsData, isUnread);

        isUnread = false;
        unreadLableObject.SetActive(false);
    }
}
