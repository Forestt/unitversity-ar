﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewsPopUp : MonoBehaviour
{
    public Text popUpNumberText;

    public void DisplayPopUp(int numberToShow)
    {
        if (numberToShow <= 0)
        {
            HidePopUp();
            return;
        }

        popUpNumberText.text = numberToShow.ToString();
        gameObject.SetActive(true);
    }

    public void HidePopUp()
    {
        gameObject.SetActive(false);
    }
}
