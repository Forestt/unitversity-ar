﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Watermelon;

[System.Serializable]
public class WatchedNewsSave
{
    public List<NewsData> newsList = new List<NewsData>();

    public WatchedNewsSave()
    {
        newsList = new List<NewsData>();
    }

    public void Save()
    {
        Serializer.SerializeToPDP(this, "CHNU_FMI_SAVE");
    }

    public static WatchedNewsSave Load()
    {
        //Serializer.DeleteFileAtPDP("CHNU_FMI_SAVE");
        return Serializer.DeserializeFromPDP<WatchedNewsSave>("CHNU_FMI_SAVE", Serializer.SerializeType.Binary, "", false);
    }
}