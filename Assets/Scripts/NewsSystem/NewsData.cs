﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NewsData
{
    public string date;
    public string topic;
    public string body;

    public string hyperLink;

    public NewsData()
    {
        date = string.Empty;
        topic = string.Empty;
        body = string.Empty;
        hyperLink = string.Empty;
    }

    public NewsData(string date, string topic, string body, string hyperLink)
    {
        this.date = date;
        this.topic = topic;
        this.body = body;
        this.hyperLink = hyperLink;
    }
}