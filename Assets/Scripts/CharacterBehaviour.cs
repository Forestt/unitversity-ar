﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBehaviour : MonoBehaviour
{
    public AppController appController;
    public AudioClip characterPhrase;
    public Animator animatorRef;

    public Gender gender;
    public bool isTalking = false;

    private Coroutine talkAnimationDisablerCoroutine;

    private bool isActive = false;

    private int startTalkingParam;
    //private int stopTalkingParam;

    private int malePoseParam;
    private int femalePoseParam;
    private int photoPoseParam;
    private int idleParam;

    private List<int> photoPosesQueue = new List<int>();

    public enum Gender
    {
        Male,
        Female,
    }

    public void Awake()
    {
        startTalkingParam = Animator.StringToHash("StartTalking");
        //stopTalkingParam = Animator.StringToHash("StopTalking");

        malePoseParam = Animator.StringToHash("MalePose");
        femalePoseParam = Animator.StringToHash("FemalePose");
        photoPoseParam = Animator.StringToHash("PhotoPose");
        idleParam = Animator.StringToHash("Idle");

        photoPosesQueue.Add(gender == Gender.Male ? malePoseParam : femalePoseParam);
        photoPosesQueue.Add(photoPoseParam);
        photoPosesQueue.Add(idleParam);
    }

    public void EnableCharacter(bool startTalking)
    {
        if (!isActive)
        {
            isActive = true;

            gameObject.SetActive(true);

            if (startTalking)
                StartTalking();
        }
    }

    public void DisableCharacter()
    {
        if (isActive)
        {
            gameObject.SetActive(false);
            StopTalking();

            if (talkAnimationDisablerCoroutine != null)
                StopCoroutine(talkAnimationDisablerCoroutine);

            isActive = false;
        }
    }

    [ContextMenu("Start talkin")]
    public void StartTalking()
    {
        isTalking = true;

        appController.PlayPhrase(characterPhrase);
        animatorRef.SetTrigger(startTalkingParam);

        talkAnimationDisablerCoroutine = StartCoroutine(TalkingAnimationBreakCoroutine());
    }

    private IEnumerator TalkingAnimationBreakCoroutine()
    {
        yield return new WaitForSeconds(characterPhrase.length);

        animatorRef.SetTrigger(idleParam);

        isTalking = false;
    }

    [ContextMenu("Stop talkin")]
    public void StopTalking()
    {
        appController.StopPhrase();
        animatorRef.SetTrigger(idleParam);

        isTalking = false;
    }


    [ContextMenu("Change pose")]
    public void ChangePose()
    {
        animatorRef.SetTrigger(photoPosesQueue[0]);

        int param = photoPosesQueue[0];
        photoPosesQueue.RemoveAt(0);
        photoPosesQueue.Add(param);
    }
}
